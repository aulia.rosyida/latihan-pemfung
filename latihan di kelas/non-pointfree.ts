
    interface Person {
        age: number;
        birthCountry: string;
        naturalizationDate: Date;
    }

    const OUR_COUNTRY = "Ireland";

    const wasBornInCountry = (person: Person) =>
        person.birthCountry === OUR_COUNTRY;

    const wasNaturalized = (person: Person) =>
        Boolean(person.naturalizationDate);

    const isOver18 = (person: Person) =>
        person.age >= 18;

    const isCitizen = (person: Person) =>
        wasBornInCountry(person) || wasNaturalized(person);

    const isEligibleToVote = (person: Person) =>
        isOver18(person) && isCitizen(person);

    isEligibleToVote({
        age: 27,
        birthCountry: "Ireland",
        naturalizationDate: new Date(),
    });
