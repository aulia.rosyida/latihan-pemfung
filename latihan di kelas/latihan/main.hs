import Data.List

sumList [] = 0
sumList (x:xs) = x + sumList xs

pembagi x n = (n `mod` x == 0) 
divisor n = [x | x <- [1 .. n], pembagi x n]


lebihkecillazy n = [ x | x <- [1 .. ], x < n]

lebihkecil n = [ x | x <- [1 .. n]]


quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs,
                                  y <= x]
                   ++ [x] ++
                   quickSort [y | y <- xs,
                                  y > x]


-- [2,3,4] - [3] = [2,4]
-- import Data.list
x = [2,3,4] \\ [3]  

perm [] = [[]]
perm ls = [ x:ps | x <- ls, ps <- perm(ls\\[x])]

pythaTriple = [(x,y,z) |  z <- [5 ..]
                        , y <- [z, z-1 .. 1]
                        , x <- [y, y-1 .. 1]
                        , x*x + y*y == z*z ]


add [] []         = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs  =  1 : 1 : add fibs (tail fibs)

primes = sieve [2 ..]
  where sieve (x:xs) = x : (sieve [z | z <- xs, z `mod` x /= 0])
