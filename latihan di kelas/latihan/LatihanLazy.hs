> import Data.List

> primes = sieve [2..]
>    where sieve (p:ps) = p : sieve [ x | x <- ps, x `mod` p /= 0 ]

> add [] []         = []
> add (a:as) (b:bs) = (a+b) : (add as bs)

> fibs  =  1 : 1 : add fibs (tail fibs)

> divisor n = [x| x <- [1 .. n] , n `mod` x ==0 ]

-- >>> 
-- >>> fibs !! 0
-- >>> fibs !! 1
-- >>> fibs !! 2
-- >>> fibs !! 3
-- >>> fibs !! 400
-- 1
-- 1
-- 2
-- 3
-- 284812298108489611757988937681460995615380088782304890986477195645969271404032323901
--

> pythaTriple = [(x,y,z) | z <- [5 ..], y <- [2 .. z-1], x <- [2 .. y-1], x*x + y*y == z*z ]
 
-- >>> :t pythaTriple 

> perms [] = [[]]
> perms ls = [ x:ps | x <- ls, ps <- perms (ls\\[x]) ] 

-- operator \\ adalah list difference, pada module Data.List, 
-- tambahkan diawal berkas
-- import Data.List 

> splits []         = [([],[])]
> splits (x:xs)     = ([],x:xs) : [(x:ps,qs) | (ps,qs)<-splits xs]

> permSplit []         = [[]]
> permSplit (x:xs)     = [ ps ++ [x] ++ qs | rs <- permSplit xs, 
>                                            (ps,qs)<- splits rs]

> quickSort []      = []
> quickSort [x]     = [x]
> quickSort (x:xs)  = quickSort [y | y <- xs, y<=x] ++ [x] ++ quickSort [y | y <- xs, y>x]


> sumList [] = 0
> sumList (x:xs) = x + sumList xs

-- >>> [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

-- >>> divisor n = [x | x <-[n, n-1 .. 1], n `mod` x == 0 ]
-- >>> divisor 12
-- [12,6,4,3,2,1]
--

-- >>> [3:xs| xs<- [[1,2],[3,5],[7,6,5],[2,3]]]
-- [[3,1,2],[3,3,5],[3,7,6,5],[3,2,3]]
--

A problem, due to the mathematician W. R. Hamming, is to write a program that produces an infinite list of numbers with the following properties:
a. The list is in ascending order, without duplicates.
b. The list begins with the number 1.
c. If the list contains the number x, 
   then it also contains the numbers 2x, 3x, and 5x.
d. The list contains no other numbers (other than mentioned above).

> merge xxs@(x:xs) yys@(y:ys) | x == y = x : merge xs ys 
>                             | x < y  = x : merge xs yys 
>                             | x>y    = y : merge xxs ys

> hamming = 1 : merge (map (2*) hamming) 
>                     (merge (map (3*) hamming)
>                            (map (5*) hamming))