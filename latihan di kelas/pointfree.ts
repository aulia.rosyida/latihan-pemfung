
    const either = <T1>(
        funcA: (a: T1) => boolean,
        funcB: (a: T1) => boolean
    ) => (arg: T1) => funcA(arg) || funcB(arg);

    const both = <T1>(
        funcA: (a: T1) => boolean,
        funcB: (a: T1) => boolean
    ) => (arg: T1) => funcA(arg) && funcB(arg);

    interface Person {
        age: number;
        birthCountry: string;
        naturalizationDate: Date;
    }

    const OUR_COUNTRY = "Ireland";

    const wasBornInCountry = (person: Person) =>
        person.birthCountry === OUR_COUNTRY;

    const wasNaturalized = (person: Person) =>
        Boolean(person.naturalizationDate);

    const isOver18 = (person: Person) =>
        person.age >= 18;

    // Pointfree style
    const isCitizen = either(wasBornInCountry, wasNaturalized);
    const isEligibleToVote = both(isOver18, isCitizen);

    isEligibleToVote({
        age: 27,
        birthCountry: "Ireland",
        naturalizationDate: new Date(),
    });
