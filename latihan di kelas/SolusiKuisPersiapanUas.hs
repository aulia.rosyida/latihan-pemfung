Functional Programming 2019 
Fasilkom UI
===========================

Contoh Solusi Kuis Persiapan UAS Soal Terkait Haskell 
=====================================================


> module SolusiKuisPersiapanUAS where  
> import Data.List 
> import qualified Data.Char as Char
> infixl 8 `dan` 

Kelas A Soal 1
--------------

Given a list of words, remove from the list all those which contain four or more vowels and those which have same letter appearing twice (or more) in a row. In addition, any word containing numbers should have the numbers removed. Note that the number removal should happen before any other operations so that the subsequent operations can remove the word if necessary. 
(source: https://www.fer.unizg.hr/)

```
weirdFilter :: [String] -> [String]
weirdFilter ["ab3c", "bananae", "fuzzy", "c1c2"]  
["abc"]
```

> notNumber x = x `notElem` "1234567890"
> numberRemoval = filter notNumber 

-- >>> numberRemoval "H90a22s32k11e312l4123l123_0i213s11_c23o72o12l"
-- "Haskell_is_cool"
--

-- >>> map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"]  
-- ["abc","bananae","fuzzy","cc"]
--



> isVowel x = x `elem` "aiueoAIUEO"
> countVowel = length . filter isVowel

-- >>> countVowel "abc"
-- 1
--

(filter vowel input)

-- >>> filter isVowel "abc"
-- "a"
-- >>> length "a"
-- 1

-- >>> length (filter isVowel  "abc")
-- 1
--

g = filter  isVowel
f = length

f(g("abc")) = (f . g) "abc"

-- >>> (length . filter isVowel ) "abc"
-- 1
--


> isThreeOrLessVowels str = countVowel str < 4

-- >>> isThreeOrLessVowels "Top Haskell"
-- False
--

-- >>> filter isThreeOrLessVowels ["ab3c", "bananae", "fuzzy", "c1c2"]  
-- ["ab3c","fuzzy","c1c2"]
--
-- >>> map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"]  
-- ["abc","bananae","fuzzy","cc"]
--
-- >>> filter isThreeOrLessVowels (map numberRemoval ["ab3c", "bananae", "fuzzy", "c1c2"])
-- ["abc","fuzzy","cc"]
--

> weirdSementara = ((filter isThreeOrLessVowels) . (map numberRemoval)) 

> weirdSementara2 = filter (isThreeOrLessVowels . numberRemoval) 


-- >>> ((filter isThreeOrLessVowels) . (map numberRemoval)) ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["abc","fuzzy","cc"]
--
-- >>> weirdSementara ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["abc","fuzzy","cc"]
--
-- >>> weirdSementara2 ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["ab3c","fuzzy","c1c2"]
--

> isNotAppearTwiceInARow (a:b:ls) | (a==b)   = False 
> isNotAppearTwiceInARow (a:ls)              = isNotAppearTwiceInARow ls 
> isNotAppearTwiceInARow []                  = True

-- >>> isNotAppearTwiceInARow "Functional Style is cool"
-- False
--

> weirdFilter = filter isThreeOrLessVowels 
>             . filter isNotAppearTwiceInARow 
>             . map numberRemoval 

-- >>> weirdFilter ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["abc"]
--

f . g = (f(g(x)))

> dan p1 p2 = (\x -> p1 x && p2 x)

> weirdFilter2 = filter (isThreeOrLessVowels `dan` isNotAppearTwiceInARow . numberRemoval)

-- >>> weirdFilter2 ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["ab3c"]
--

> weirdFilter3 = filter (isThreeOrLessVowels `dan` isNotAppearTwiceInARow) 
>                . (map numberRemoval)

-- >>> weirdFilter3 ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["abc"]
--

Kelas A Soal 2
--------------
Write a function rotabc that changes a's to b's, b's to c's and c's to a's in a string. Only lowercase letters are affected. (source: https://www2.cs.arizona.edu/classes/cs372/spring14)

```
rotabc :: String -> String
rotabc "Bananae"  
Bbnbnbe
```
rotabc (x:xs) | x == 'a' = 'b' : rotabs xs 
              | x == 'b' = 'c' : rotabs xs 
              | x == 'c' = 'a' : rotabs xs  
              | _  = x : rotabs xs

> rotabc = map abc 
>   where abc 'a' = 'b'
>         abc 'b' = 'c'
>         abc 'c' = 'a'
>         abc  x  =  x

-- >>> rotabc "scyc sukc hcskell"
-- "saya suka haskell"
--

Kelas A Soal 3
--------------

Definisikan fungsi last, dengan menerapkan point-free style. Fungsi last tersebut menerima sebuah list  dan mengembalikan elemen terakhir dari list tersebut.

> last = head . reverse

-- >>> last "Haskell"
-- 'd'
--


Kelas B Soal 1
--------------

Given a sentence, define a function called capitalise which returns the same sentence with all words capitalised except the ones from a given list.
(source: https://www.fer.unizg.hr/)
capitalise :: String -> [String] -> String
capitalise "this is a sentence." ["a", "is"]
"This is a Sentence."

> getWord word [] = (word,[])
> getWord word (x:xs) | x == ' ' = (word, xs)
>                     | x /= ' ' = getWord (word ++ [x]) xs  

-- >>> getWord [] "this is a sentence"
-- ("this","is a sentence")
--

> splitter lw []  = lw
> splitter lw inp = let (word, rest) = getWord "" inp
>                   in splitter (lw ++ [word]) rest 
                  
-- >>> splitter [] "this is a sentence."
-- ["this","is","a","sentence."]
--

-- >>> Char.toUpper 'a'
-- 'A'
--

> upper el word = if word `elem` el 
>                 then word 
>                 else Char.toUpper (head word) : (tail word)

> upper2 el word@(h:t) = if word `elem` el 
>                        then word 
>                        else Char.toUpper h : t


-- >>> upper ["is","a"] "this"
-- "This"
--

> combine []  = []
> combine [a] = a
> combine (a:xs) = a ++ " " ++ (combine xs)

-- >>> combine ["this","is","a","sentence."]
-- "this is a sentence."
--

> capitalise el inp = let lw = splitter [] inp
>                      in combine (map (upper el) lw) 

capitalise el inp = 
                    combine (map (upper el) (splitter [] inp)) 

f = combine
g = map (upper el)
h = splitter []

f(g(h(inp))) = (f . g . h) inp

capitalise el  =  combine . (map (upper el)) . (splitter []) 


-- >>> capitalise "this is a sentence." ["a", "is"]
-- "This is a Sentence. "
--
