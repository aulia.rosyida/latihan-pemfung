import Data.List
-- 1. Uraikan langkah evaluasi dari ekspresi berikut: [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]
no1 = [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

{-- jawaban:
x = 1 | 2 | 3 | 4
y = 2 | 3 | 4

yang sesuai dengan predikat x > y yaitu:
x = 3   x=4     x=4
y = 2   y=2     y=3

Kemudian tinjau satu per satu
(3+2) : [x+y | ..]
(3+2) : (4+2) : [x+y | ..]
(3+2) : (4+2) : (4+3) : [x+y | ..]
(3+2) : (4+2) : (4+3) : []
= [5,6,7]
-}

-- 2. Buatlah fungsi divisor yang menerima sebuah bilangan bulat n dan mengembalikan
-- list bilangan bulat positif yang membagi habis n, Contoh:
-- LatihanLazy> divisor 12
-- [1,2,3,4,6,12]
divisor n = [x | x <-[1..n], n `mod` x == 0 ]
divisor' n = reverse[x| x<- [n, n-1 .. 1], n `mod` x == 0]

-- 3. Buatlah definisi quick sort menggunakan list comprehension.
quickSort [] = []
quickSort (x:xs) = quickSort[y| y<- xs, y <= x ] ++ [x] ++ quickSort[ y | y <- xs, y > x]

-- 4. Buatlah definisi infinite list untuk permutation.
perms [] = [[]]
perms ls = [ x : sisa | x <- ls, sisa <- perms (ls \\ [x]) ]

-- 5. Buatlah definisi untuk memberikan infinite list dari bilangan prima menerapkan algoritma Sieve of Erastothenes.
primes = sieve [2..]
    where
        sieve (x:xs) = x: sieve [y| y <- xs, y `mod` x /= 0 ]

-- 6. Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari element
-- triple bilangan bulat positif yang mengikut persamaan pythagoras x
-- 2 + y
-- 2 = z
-- 2
-- .
-- Contoh:
-- LatihanLazy > pythaTriple
-- [(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17),(12,16,20) … ]
-- Perhatian urutan penyusun list comprehension nya, coba mulai dari variable z!

pythaTriple = [ (x, y, z) | z<-[5..],
                            y<-[z, z-1 .. 1],
                            x<-[y, y-1 .. 1], x^2 + y^2 == z^2]

mystery xs ys = concat( map (\x -> map( \y -> (x,y)) ys) xs)
mystery' xs ys = map (\x -> map( \y -> (x,y)) ys) xs
mystery'' ys = map( \y -> (y)) ys

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = \x y -> f y x
