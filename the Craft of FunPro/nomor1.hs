-- A. Define the length function using map and sum.
length':: [a2] -> Int
length' xs = sum ( map ( \x -> 1 ) xs )

--versi2
length'' xs = sum[1| _ <- xs]

--B. What does map (+1) (map (+1) xs) do? 
--Jawab: memetakan penambahan 1 ke tiap elemen list, kemudian memetakan lagi penambahan 1 ke tiap elemen list
--       = menambahkan 2 ke tiap elemen dari list
peta xs = map (+1) xs
peta' xs = map (+1) (map (+1) xs)

--Can you conclude anything in general about properties of :
-- map f (map g xs) where f and g are arbitrary functions?
-- memetakan x ke fungsi g, kemudian hasilnya dipetakan lagi ke fungsi f 

--C. Give the type of, and define the function iter so that
-- iter n f x = f (f (... (f x)))
iter :: Int -> (a -> a) -> ( a -> a)
-- where f occurs n times on the right-hand side of the equation. 
--For instance, we should have
-- iter 3 f x = f (f (f x))
-- and iter 0 f x should return x.
iter 0 f x = x
-- iter 1 f x = f (x)
-- iter 2 f x = f (f (x))
-- iter 3 f x = f (f (f (x)))
iter n f x = f (iter (n-1) f x) 

--alternatif jawaban:
iter' :: Int -> (a -> a) -> (a -> a)
iter' 0 f = id
iter' n f = f . iter (n-1) f

{--What is the type and effect of the following function?
where succ is the successor function, which increases a value by one -}
-- \n -> (a -> a) => a -> (a -> a) -> a
-- \n -> iter n succ

-- prop_IterN x y =
--     x >= 0 && y >= 0 ==>
--       f1 x y == f2 x y
--    where
--     f1 = (\n -> iter n succ) :: Int -> Int -> Int
--     f2 = (+)

{--How would you define the sum of the squares of the natural numbers 1 to n using map and foldr?
-}
sumSquares :: Integer -> Integer
sumSquares2 :: Integer -> Integer
sumSquares n = foldr (+) 0 $ map (\x -> x*x) [1..n]
sumSquares2 n = foldr (+) 0 $ map (^2) [1..n]

{--How does the function behave?-}

mystery xs = foldr (++) [] (map sing xs)
  where
    sing x = [x]

-- jumlahList [] = 0
jumlahList xs = foldr (+) 0 xs
jumlahList2 xs = foldl (+) 0 xs

maxTiga :: Int -> Int -> Int -> Int
maxTiga a b c = maximum[a,b,c]

maxTiga2 a b c = max a (max b c)

