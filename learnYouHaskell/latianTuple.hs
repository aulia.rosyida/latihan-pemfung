--tuple mirip dg list
--perbedaannya, tuple bisa berisi beda-beda type, sedangkan list ga bisa
--list of tuple, jumlah isi di dalam tuplenya harus fix, gabisa beda-beda, sedangkan list bisa beda

--pake tuple apabila jumlah dari bagian datanya sudah fix
--gaada fungsi iumum untuk menambahkan elemen pada tuple, musti tulis fungsi sendiri
--gaada yang namanya singleton tuple, karena hanya punya nilai yang di dalamnya

--tuples dapat dicompare dg tuples lainnya jika komponen2nya 1 tipe
--tapi, gabisa compare 2 tuples yang memiliki ukuran berbeda

--combines tuples dan list comprehension
triangles = [ (a,b,c) | a <- [1..10], b <-[1..10], c <-[1..10] ]

rightTriangles = [ (a,b,c) | c <- [1..10], 
                             b <-[1..c], 
                             a <-[1..b],  a^2 + b^2 == c^2]

--perimeternya 24 saja
rightTriangles' = [ (a,b,c) | c <- [1..10], 
                             b <-[1..c], 
                             a <-[1..b],  a^2 + b^2 == c^2, a+b+c == 24]                             