-- fungsi juga memiliki type
--ketika nulis fungsi, kita dapat memilih untuk memberikan eksplisit type of declaration
--hal tsb juga merupakan good practice kecuali menulis fungsi yang sangat pendek

-- == removeHurufKecil :: [Char] -> [Char]
removeHurufKecil :: String -> String
removeHurufKecil st = [ c |c <- st, c `elem` ['A'..'Z']]

-- add3 :: Int -> Int -> Int -> Int
add3 :: Int -> Int -> Int -> Int
add3 x y z = x + y + z

keliling :: Float -> Float
--keliling :: Double -> Double
-- ERROR: keliling :: Float -> Double
-- ERROR: keliling :: Double -> Float
keliling r = 2 * pi * r

-- Fungsi yang punya type var disebut POLYMORPHIC FUNCTION

-- class constraints => 
-- function :: (typeclass var) => var -> var -> TypeData

--ERROR: coba bikin definisi fungsi terima bilangan bulat positif
-- newtype Positive a = Positive a

-- toPositive ::(Num a, Ord a) => a -> Positive a
-- toPositive x
--     | x < 0 = error "number cannot be negative"
--     | otherwise = Positive x

-- max3 :: Positive a => a -> a -> a -> a
-- max3 (Positive d) (Positive b) (Positive c) = maximum(d,b,c)

-- max33 :: int -> int -> int -> int
-- max33 a b c = maximum[a,b,c]

--coba pake natural
-- max3 :: Ord a => a -> a -> a -> a
-- max3 a b c = [x | x <- [maximum[a,b,c]], x>0]
max3 a b c = maximum[a,b,c]
