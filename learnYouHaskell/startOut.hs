-- belajar pada link:
-- http://learnyouahaskell.com/starting-out#babys-first-functions

-- dapat memanggil fungsi dari fungsi lain yang sudah dibuat
-- dengan jalan ini, dapat menghindari repetisi (pengulangan)
-- jika butuh mengganti program, dapat redefine program dasarnya saja
redefineD2 x y = bikinDouble x + bikinDouble y

-- Fungsi yang menerima angka dan hasilnya  dikalikan dua
bikinDouble x = x + x + x

--fungsi yang ambil 2 angka dan masing-masing dikali 2 dan dijumlahkan
double2 x y = x*2 + y*2

-- fungsi yang mengalikan angka dengan 2, tapi jika si angka <= 100
-- if-statement akan menggunakan else sebagai mandatory.
-- sehingga termasuk ekspresi (dasarnya adalah potongan kode yang menggunakan ekspresi)
doubleAngkaKecil x = if x > 100
                     then x 
                     else x*2

doubleAngkaKecil' x = doubleAngkaKecil x + 1

-- nama fungsi tidak dapat dimulai dengan huruf kapital.
-- fungsi yang tidak menerima param apa-apa, biasanya disebut dengan definisi (atau sebuah nama)
-- karena sekali didefined, maka dapat digunakan interchangeably
auliaO'Rosyida = "It's a-me, Aulia O'Rosyida!"

-- list comprehension
y = [1..10]

--dengan menggunakan predikat pada suatu list disebut juga filtering
boomBeng xs = [ if x < 10 then "BOM" else "BENG BENG" | x <- xs, odd x]

--dapat menggunakan multiple predikat
not1315 xs = [x | x <- xs, x/=13, x/=15, x/=17]

--dapat menggunakan lebih dari 1 list untuk di tinjau
allComb xs ys = [x*y |x<-xs, y<-ys, x*y > 50 ]

--list comprehension yang mengombinasikan list kata sifat dan kata benda
adjNoun adj no = [adjec ++ " " ++ noun | adjec<-adj, noun<-no]

-- _ berarti kita ga peduli apa yang akan diambil dari list
--fungsi dibawah ini mengganti tiap element list dengan 1 dan menambahkannya
--hasil sumnya adalah panjang dari list tsb
length' xs = sum[1| _ <- xs]

--string adalah list, bisa pake list comprehesion untuk process dan produce strings
--fungsi dibawah hapus yang bukan huruf kapital
removeNonUpperCase st = [st2 | st2 <- st, st2 `elem` ['A'..'Z']]

--nested list comprehension juga bisa dilakukan dalam operasikan list yang ada di dalam list
--fungsi untuk hapus semua angka ganjil tanpa membuat list mjd 1
listOp xss = [ [ x | x <- xs, even x] | xs <- xss]